import { _decorator, Component, Node } from 'cc';
import { BaseController } from './BaseController';

const { ccclass, property } = _decorator;

@ccclass('GameUIController')
export class GameUIController extends BaseController {
    start() {

    }

    update(deltaTime: number) {
        
    }
}

