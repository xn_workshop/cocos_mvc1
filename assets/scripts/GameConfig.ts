import { AudioClip, Prefab } from "cc";

const MainGameRes = {
    "Audio": AudioClip,
    "GUI": [
        {
            assetType: Prefab,
            urls: [
                "UIPrefab/Background",
                "UIPrefab/LoginUI"
            ]
        }
    ]
}

const FreeGameRes = {

}

export { MainGameRes, FreeGameRes }