import { AudioManager } from "./AudioManager"
import { FREESPIN_AUDIO } from "./AudioConstant";
import { ResManager } from "../ResManager";

export class FreeSpinAudioManager extends AudioManager {
    
    public static get Instance(): FreeSpinAudioManager {
        return FreeSpinAudioManager.instance!;
    }

    protected onLoad(): void {
        if(!FreeSpinAudioManager.instance) {
            FreeSpinAudioManager.instance = this;
        } else {
            this.destroy();
            return;
        }
        const audioClip = ResManager.Instance.getAsset("Audio", "FREESPIN_AUDIO")
        this.loadAudioClip(audioClip, FREESPIN_AUDIO);
    }

}