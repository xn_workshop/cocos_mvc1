import { _decorator, Component, log, Node } from 'cc';
import { GeneralAudioManager } from '../services/audio/GeneralAudioManager';
import { MainGameAudioManager } from '../services/audio/MainGameAudioManager';
import { ResManager } from '../services/ResManager';
import { MainGameRes } from '../GameConfig';
import { GENERAL_AUDIO, MAINGAME_AUDIO } from '../services/audio/AudioConstant';
import { BGMAudioManager } from '../services/audio/BGMAudioManager';
import { AudioManager } from '../services/audio/AudioManager';
import { TimerManager } from '../services/TimerManager';
import { UIManager } from '../services/UIManager';
import { ProtoManager } from '../services/net/ProtoManager';

export class GameApp extends Component {

    private static instance: GameApp | null = null;

    public static get Instance(): GameApp {
        return GameApp.instance!;
    }

    protected onLoad(): void {
        if(!GameApp.instance) {
            GameApp.instance = this;
        } else {
            this.destroy();
            return;
        }
    }

    /**
     * 進入遊戲
     */
    public enterGame(config: object): void {
        ResManager.Instance.preloadResPkg(MainGameRes, (now: any, total: any) => {
            console.log(`now: ${now}`, `total: ${total}`);
        }, () => {
            this.node.addComponent(BGMAudioManager);
            this.node.addComponent(MainGameAudioManager);
            this.loadingScene();
        })
    }

    /**
     * 載入場景
     */
    protected loadingScene() {
        UIManager.Instance.showUIView("Background");
        UIManager.Instance.showUIView("LoginUI");
        // 實例化相關物件
        log("loadingScene.....");
        // BGMAudioManager.Instance.play().loop();
        // this.scheduleOnce(()=>{
        //     AudioManager.Mute();
        // }, 3);

        // this.scheduleOnce(()=>{
        //     AudioManager.Mute(false);
        // }, 6);
        // TimerManager.Instance.Schedule((data)=>{
        //     console.log('timers: ', data);
        // }, 10, 2, 0, ['hi', 'xuan']);

        // const author: Author = {
        //     name: "xuan",
        //     role: "admin"
        // };
        // const buf = ProtoManager.Instance.serializeMsg("Author", author);
        // console.log(`buf: ${buf}`);
        // const _author = ProtoManager.Instance.deserializeMsg("Author", buf) as Author;
        // console.log(`author: ${_author.name}`);
        // const author = new Author();
        // author.setName("Xuan");
        // console.log(author);
        // author.setName("Xuan");
        // author.setRole("admin");
        // const buf = author.serializeBinary();
        // console.log(`buf: ${buf}`);
        
        const author = new model.Author();
        author.name = "Xuan";
        author.role = "admin";
        const buf = model.Author.encode(author).finish();
        console.log("author: ", author.constructor.name, "buf: ", buf);
        
    }

    /**
     * 登入畫面
     */
    protected loadingLogin() {

    }
}

