/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
(window || global).dm = (function($protobuf) {
    "use strict";

    // Common aliases
    var $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;
    
    // Exported root namespace
    var $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});
    
    $root.pb = (function() {
    
        /**
         * Namespace pb.
         * @exports pb
         * @namespace
         */
        var pb = {};
    
        pb.Author = (function() {
    
            /**
             * Properties of an Author.
             * @memberof pb
             * @interface IAuthor
             * @property {string|null} [name] Author name
             * @property {string|null} [role] Author role
             */
    
            /**
             * Constructs a new Author.
             * @memberof pb
             * @classdesc Represents an Author.
             * @implements IAuthor
             * @constructor
             * @param {pb.IAuthor=} [properties] Properties to set
             */
            function Author(properties) {
                if (properties)
                    for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                        if (properties[keys[i]] != null)
                            this[keys[i]] = properties[keys[i]];
            }
    
            /**
             * Author name.
             * @member {string} name
             * @memberof pb.Author
             * @instance
             */
            Author.prototype.name = "";
    
            /**
             * Author role.
             * @member {string} role
             * @memberof pb.Author
             * @instance
             */
            Author.prototype.role = "";
    
            /**
             * Creates a new Author instance using the specified properties.
             * @function create
             * @memberof pb.Author
             * @static
             * @param {pb.IAuthor=} [properties] Properties to set
             * @returns {pb.Author} Author instance
             */
            Author.create = function create(properties) {
                return new Author(properties);
            };
    
            /**
             * Encodes the specified Author message. Does not implicitly {@link pb.Author.verify|verify} messages.
             * @function encode
             * @memberof pb.Author
             * @static
             * @param {pb.IAuthor} message Author message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            Author.encode = function encode(message, writer) {
                if (!writer)
                    writer = $Writer.create();
                if (message.name != null && Object.hasOwnProperty.call(message, "name"))
                    writer.uint32(/* id 1, wireType 2 =*/10).string(message.name);
                if (message.role != null && Object.hasOwnProperty.call(message, "role"))
                    writer.uint32(/* id 2, wireType 2 =*/18).string(message.role);
                return writer;
            };
    
            /**
             * Encodes the specified Author message, length delimited. Does not implicitly {@link pb.Author.verify|verify} messages.
             * @function encodeDelimited
             * @memberof pb.Author
             * @static
             * @param {pb.IAuthor} message Author message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            Author.encodeDelimited = function encodeDelimited(message, writer) {
                return this.encode(message, writer).ldelim();
            };
    
            /**
             * Decodes an Author message from the specified reader or buffer.
             * @function decode
             * @memberof pb.Author
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @param {number} [length] Message length if known beforehand
             * @returns {pb.Author} Author
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            Author.decode = function decode(reader, length) {
                if (!(reader instanceof $Reader))
                    reader = $Reader.create(reader);
                var end = length === undefined ? reader.len : reader.pos + length, message = new $root.pb.Author();
                while (reader.pos < end) {
                    var tag = reader.uint32();
                    switch (tag >>> 3) {
                    case 1:
                        message.name = reader.string();
                        break;
                    case 2:
                        message.role = reader.string();
                        break;
                    default:
                        reader.skipType(tag & 7);
                        break;
                    }
                }
                return message;
            };
    
            /**
             * Decodes an Author message from the specified reader or buffer, length delimited.
             * @function decodeDelimited
             * @memberof pb.Author
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @returns {pb.Author} Author
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            Author.decodeDelimited = function decodeDelimited(reader) {
                if (!(reader instanceof $Reader))
                    reader = new $Reader(reader);
                return this.decode(reader, reader.uint32());
            };
    
            /**
             * Verifies an Author message.
             * @function verify
             * @memberof pb.Author
             * @static
             * @param {Object.<string,*>} message Plain object to verify
             * @returns {string|null} `null` if valid, otherwise the reason why it is not
             */
            Author.verify = function verify(message) {
                if (typeof message !== "object" || message === null)
                    return "object expected";
                if (message.name != null && message.hasOwnProperty("name"))
                    if (!$util.isString(message.name))
                        return "name: string expected";
                if (message.role != null && message.hasOwnProperty("role"))
                    if (!$util.isString(message.role))
                        return "role: string expected";
                return null;
            };
    
            /**
             * Creates an Author message from a plain object. Also converts values to their respective internal types.
             * @function fromObject
             * @memberof pb.Author
             * @static
             * @param {Object.<string,*>} object Plain object
             * @returns {pb.Author} Author
             */
            Author.fromObject = function fromObject(object) {
                if (object instanceof $root.pb.Author)
                    return object;
                var message = new $root.pb.Author();
                if (object.name != null)
                    message.name = String(object.name);
                if (object.role != null)
                    message.role = String(object.role);
                return message;
            };
    
            /**
             * Creates a plain object from an Author message. Also converts values to other types if specified.
             * @function toObject
             * @memberof pb.Author
             * @static
             * @param {pb.Author} message Author
             * @param {$protobuf.IConversionOptions} [options] Conversion options
             * @returns {Object.<string,*>} Plain object
             */
            Author.toObject = function toObject(message, options) {
                if (!options)
                    options = {};
                var object = {};
                if (options.defaults) {
                    object.name = "";
                    object.role = "";
                }
                if (message.name != null && message.hasOwnProperty("name"))
                    object.name = message.name;
                if (message.role != null && message.hasOwnProperty("role"))
                    object.role = message.role;
                return object;
            };
    
            /**
             * Converts this Author to JSON.
             * @function toJSON
             * @memberof pb.Author
             * @instance
             * @returns {Object.<string,*>} JSON object
             */
            Author.prototype.toJSON = function toJSON() {
                return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
            };
    
            return Author;
        })();
    
        pb.Change = (function() {
    
            /**
             * Properties of a Change.
             * @memberof pb
             * @interface IChange
             * @property {pb.Kind|null} [kind] Change kind
             * @property {string|null} [patch] Change patch
             * @property {Array.<string>|null} [tags] Change tags
             * @property {string|null} [name] Change name
             * @property {string|null} [id] Change id
             * @property {pb.IAuthor|null} [author] Change author
             */
    
            /**
             * Constructs a new Change.
             * @memberof pb
             * @classdesc Represents a Change.
             * @implements IChange
             * @constructor
             * @param {pb.IChange=} [properties] Properties to set
             */
            function Change(properties) {
                this.tags = [];
                if (properties)
                    for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                        if (properties[keys[i]] != null)
                            this[keys[i]] = properties[keys[i]];
            }
    
            /**
             * Change kind.
             * @member {pb.Kind} kind
             * @memberof pb.Change
             * @instance
             */
            Change.prototype.kind = 0;
    
            /**
             * Change patch.
             * @member {string} patch
             * @memberof pb.Change
             * @instance
             */
            Change.prototype.patch = "";
    
            /**
             * Change tags.
             * @member {Array.<string>} tags
             * @memberof pb.Change
             * @instance
             */
            Change.prototype.tags = $util.emptyArray;
    
            /**
             * Change name.
             * @member {string|null|undefined} name
             * @memberof pb.Change
             * @instance
             */
            Change.prototype.name = null;
    
            /**
             * Change id.
             * @member {string|null|undefined} id
             * @memberof pb.Change
             * @instance
             */
            Change.prototype.id = null;
    
            /**
             * Change author.
             * @member {pb.IAuthor|null|undefined} author
             * @memberof pb.Change
             * @instance
             */
            Change.prototype.author = null;
    
            // OneOf field names bound to virtual getters and setters
            var $oneOfFields;
    
            /**
             * Change nameOrId.
             * @member {"name"|"id"|undefined} nameOrId
             * @memberof pb.Change
             * @instance
             */
            Object.defineProperty(Change.prototype, "nameOrId", {
                get: $util.oneOfGetter($oneOfFields = ["name", "id"]),
                set: $util.oneOfSetter($oneOfFields)
            });
    
            /**
             * Creates a new Change instance using the specified properties.
             * @function create
             * @memberof pb.Change
             * @static
             * @param {pb.IChange=} [properties] Properties to set
             * @returns {pb.Change} Change instance
             */
            Change.create = function create(properties) {
                return new Change(properties);
            };
    
            /**
             * Encodes the specified Change message. Does not implicitly {@link pb.Change.verify|verify} messages.
             * @function encode
             * @memberof pb.Change
             * @static
             * @param {pb.IChange} message Change message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            Change.encode = function encode(message, writer) {
                if (!writer)
                    writer = $Writer.create();
                if (message.kind != null && Object.hasOwnProperty.call(message, "kind"))
                    writer.uint32(/* id 1, wireType 0 =*/8).int32(message.kind);
                if (message.patch != null && Object.hasOwnProperty.call(message, "patch"))
                    writer.uint32(/* id 2, wireType 2 =*/18).string(message.patch);
                if (message.tags != null && message.tags.length)
                    for (var i = 0; i < message.tags.length; ++i)
                        writer.uint32(/* id 3, wireType 2 =*/26).string(message.tags[i]);
                if (message.name != null && Object.hasOwnProperty.call(message, "name"))
                    writer.uint32(/* id 4, wireType 2 =*/34).string(message.name);
                if (message.id != null && Object.hasOwnProperty.call(message, "id"))
                    writer.uint32(/* id 5, wireType 2 =*/42).string(message.id);
                if (message.author != null && Object.hasOwnProperty.call(message, "author"))
                    $root.pb.Author.encode(message.author, writer.uint32(/* id 6, wireType 2 =*/50).fork()).ldelim();
                return writer;
            };
    
            /**
             * Encodes the specified Change message, length delimited. Does not implicitly {@link pb.Change.verify|verify} messages.
             * @function encodeDelimited
             * @memberof pb.Change
             * @static
             * @param {pb.IChange} message Change message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            Change.encodeDelimited = function encodeDelimited(message, writer) {
                return this.encode(message, writer).ldelim();
            };
    
            /**
             * Decodes a Change message from the specified reader or buffer.
             * @function decode
             * @memberof pb.Change
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @param {number} [length] Message length if known beforehand
             * @returns {pb.Change} Change
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            Change.decode = function decode(reader, length) {
                if (!(reader instanceof $Reader))
                    reader = $Reader.create(reader);
                var end = length === undefined ? reader.len : reader.pos + length, message = new $root.pb.Change();
                while (reader.pos < end) {
                    var tag = reader.uint32();
                    switch (tag >>> 3) {
                    case 1:
                        message.kind = reader.int32();
                        break;
                    case 2:
                        message.patch = reader.string();
                        break;
                    case 3:
                        if (!(message.tags && message.tags.length))
                            message.tags = [];
                        message.tags.push(reader.string());
                        break;
                    case 4:
                        message.name = reader.string();
                        break;
                    case 5:
                        message.id = reader.string();
                        break;
                    case 6:
                        message.author = $root.pb.Author.decode(reader, reader.uint32());
                        break;
                    default:
                        reader.skipType(tag & 7);
                        break;
                    }
                }
                return message;
            };
    
            /**
             * Decodes a Change message from the specified reader or buffer, length delimited.
             * @function decodeDelimited
             * @memberof pb.Change
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @returns {pb.Change} Change
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            Change.decodeDelimited = function decodeDelimited(reader) {
                if (!(reader instanceof $Reader))
                    reader = new $Reader(reader);
                return this.decode(reader, reader.uint32());
            };
    
            /**
             * Verifies a Change message.
             * @function verify
             * @memberof pb.Change
             * @static
             * @param {Object.<string,*>} message Plain object to verify
             * @returns {string|null} `null` if valid, otherwise the reason why it is not
             */
            Change.verify = function verify(message) {
                if (typeof message !== "object" || message === null)
                    return "object expected";
                var properties = {};
                if (message.kind != null && message.hasOwnProperty("kind"))
                    switch (message.kind) {
                    default:
                        return "kind: enum value expected";
                    case 0:
                    case 1:
                        break;
                    }
                if (message.patch != null && message.hasOwnProperty("patch"))
                    if (!$util.isString(message.patch))
                        return "patch: string expected";
                if (message.tags != null && message.hasOwnProperty("tags")) {
                    if (!Array.isArray(message.tags))
                        return "tags: array expected";
                    for (var i = 0; i < message.tags.length; ++i)
                        if (!$util.isString(message.tags[i]))
                            return "tags: string[] expected";
                }
                if (message.name != null && message.hasOwnProperty("name")) {
                    properties.nameOrId = 1;
                    if (!$util.isString(message.name))
                        return "name: string expected";
                }
                if (message.id != null && message.hasOwnProperty("id")) {
                    if (properties.nameOrId === 1)
                        return "nameOrId: multiple values";
                    properties.nameOrId = 1;
                    if (!$util.isString(message.id))
                        return "id: string expected";
                }
                if (message.author != null && message.hasOwnProperty("author")) {
                    var error = $root.pb.Author.verify(message.author);
                    if (error)
                        return "author." + error;
                }
                return null;
            };
    
            /**
             * Creates a Change message from a plain object. Also converts values to their respective internal types.
             * @function fromObject
             * @memberof pb.Change
             * @static
             * @param {Object.<string,*>} object Plain object
             * @returns {pb.Change} Change
             */
            Change.fromObject = function fromObject(object) {
                if (object instanceof $root.pb.Change)
                    return object;
                var message = new $root.pb.Change();
                switch (object.kind) {
                case "UPDATED":
                case 0:
                    message.kind = 0;
                    break;
                case "DELETED":
                case 1:
                    message.kind = 1;
                    break;
                }
                if (object.patch != null)
                    message.patch = String(object.patch);
                if (object.tags) {
                    if (!Array.isArray(object.tags))
                        throw TypeError(".pb.Change.tags: array expected");
                    message.tags = [];
                    for (var i = 0; i < object.tags.length; ++i)
                        message.tags[i] = String(object.tags[i]);
                }
                if (object.name != null)
                    message.name = String(object.name);
                if (object.id != null)
                    message.id = String(object.id);
                if (object.author != null) {
                    if (typeof object.author !== "object")
                        throw TypeError(".pb.Change.author: object expected");
                    message.author = $root.pb.Author.fromObject(object.author);
                }
                return message;
            };
    
            /**
             * Creates a plain object from a Change message. Also converts values to other types if specified.
             * @function toObject
             * @memberof pb.Change
             * @static
             * @param {pb.Change} message Change
             * @param {$protobuf.IConversionOptions} [options] Conversion options
             * @returns {Object.<string,*>} Plain object
             */
            Change.toObject = function toObject(message, options) {
                if (!options)
                    options = {};
                var object = {};
                if (options.arrays || options.defaults)
                    object.tags = [];
                if (options.defaults) {
                    object.kind = options.enums === String ? "UPDATED" : 0;
                    object.patch = "";
                    object.author = null;
                }
                if (message.kind != null && message.hasOwnProperty("kind"))
                    object.kind = options.enums === String ? $root.pb.Kind[message.kind] : message.kind;
                if (message.patch != null && message.hasOwnProperty("patch"))
                    object.patch = message.patch;
                if (message.tags && message.tags.length) {
                    object.tags = [];
                    for (var j = 0; j < message.tags.length; ++j)
                        object.tags[j] = message.tags[j];
                }
                if (message.name != null && message.hasOwnProperty("name")) {
                    object.name = message.name;
                    if (options.oneofs)
                        object.nameOrId = "name";
                }
                if (message.id != null && message.hasOwnProperty("id")) {
                    object.id = message.id;
                    if (options.oneofs)
                        object.nameOrId = "id";
                }
                if (message.author != null && message.hasOwnProperty("author"))
                    object.author = $root.pb.Author.toObject(message.author, options);
                return object;
            };
    
            /**
             * Converts this Change to JSON.
             * @function toJSON
             * @memberof pb.Change
             * @instance
             * @returns {Object.<string,*>} JSON object
             */
            Change.prototype.toJSON = function toJSON() {
                return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
            };
    
            return Change;
        })();
    
        /**
         * Kind enum.
         * @name pb.Kind
         * @enum {number}
         * @property {number} UPDATED=0 UPDATED value
         * @property {number} DELETED=1 DELETED value
         */
        pb.Kind = (function() {
            var valuesById = {}, values = Object.create(valuesById);
            values[valuesById[0] = "UPDATED"] = 0;
            values[valuesById[1] = "DELETED"] = 1;
            return values;
        })();
    
        return pb;
    })();

    return $root;
})(protobuf).dm;