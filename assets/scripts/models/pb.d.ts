declare global {
 // DO NOT EDIT! This is a generated file. Edit the JSDoc in src/*.js instead and run 'npm run types'.

/** Namespace pb. */
export namespace pb {

    /** Properties of an Author. */
    interface IAuthor {

        /** Author name */
        name?: (string|null);

        /** Author role */
        role?: (string|null);
    }

    /** Represents an Author. */
    class Author implements IAuthor {

        /**
         * Constructs a new Author.
         * @param [properties] Properties to set
         */
        constructor(properties?: pb.IAuthor);

        /** Author name. */
        public name: string;

        /** Author role. */
        public role: string;

        /**
         * Creates a new Author instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Author instance
         */
        public static create(properties?: pb.IAuthor): pb.Author;

        /**
         * Encodes the specified Author message. Does not implicitly {@link pb.Author.verify|verify} messages.
         * @param message Author message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: pb.IAuthor, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified Author message, length delimited. Does not implicitly {@link pb.Author.verify|verify} messages.
         * @param message Author message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: pb.IAuthor, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes an Author message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Author
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): pb.Author;

        /**
         * Decodes an Author message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Author
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): pb.Author;

        /**
         * Verifies an Author message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates an Author message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns Author
         */
        public static fromObject(object: { [k: string]: any }): pb.Author;

        /**
         * Creates a plain object from an Author message. Also converts values to other types if specified.
         * @param message Author
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: pb.Author, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this Author to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a Change. */
    interface IChange {

        /** Change kind */
        kind?: (pb.Kind|null);

        /** Change patch */
        patch?: (string|null);

        /** Change tags */
        tags?: (string[]|null);

        /** Change name */
        name?: (string|null);

        /** Change id */
        id?: (string|null);

        /** Change author */
        author?: (pb.IAuthor|null);
    }

    /** Represents a Change. */
    class Change implements IChange {

        /**
         * Constructs a new Change.
         * @param [properties] Properties to set
         */
        constructor(properties?: pb.IChange);

        /** Change kind. */
        public kind: pb.Kind;

        /** Change patch. */
        public patch: string;

        /** Change tags. */
        public tags: string[];

        /** Change name. */
        public name?: (string|null);

        /** Change id. */
        public id?: (string|null);

        /** Change author. */
        public author?: (pb.IAuthor|null);

        /** Change nameOrId. */
        public nameOrId?: ("name"|"id");

        /**
         * Creates a new Change instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Change instance
         */
        public static create(properties?: pb.IChange): pb.Change;

        /**
         * Encodes the specified Change message. Does not implicitly {@link pb.Change.verify|verify} messages.
         * @param message Change message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: pb.IChange, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified Change message, length delimited. Does not implicitly {@link pb.Change.verify|verify} messages.
         * @param message Change message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: pb.IChange, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a Change message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Change
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): pb.Change;

        /**
         * Decodes a Change message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Change
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): pb.Change;

        /**
         * Verifies a Change message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a Change message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns Change
         */
        public static fromObject(object: { [k: string]: any }): pb.Change;

        /**
         * Creates a plain object from a Change message. Also converts values to other types if specified.
         * @param message Change
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: pb.Change, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this Change to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Kind enum. */
    enum Kind {
        UPDATED = 0,
        DELETED = 1
    }
}
 
} 
 export {}